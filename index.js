// Initialising discord basic config
const Discord = require('discord.js');
const bot = new Discord.Client();

// importing librairies
const ytdl = require("ytdl-core");
const fs = require('fs');

// Creating commands collection, config and parsing
bot.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands/').filter(file => file.endsWith('.js'));
for(const file of commandFiles){
    const command = require(`./commands/${file}`);
    bot.commands.set(command.name, command);
}

// Get user token
var conf = require("./.conf");
const token = conf.token;

// Setting bot prefix
const PREFIX = '$';

// Bot version
const version = '1.0.1';
const versionState = 'not stable';

// Initialising servers container
var servers = {};

// Startup message with version printing
bot.on('ready', () => {
    console.log('Bot is online ! running core version => ' + version + '  |  state : ' + versionState);
})

// Greatings for new member arrival
bot.on('guildMemberAdd', member =>{
    bot.commands.get('newmember').execute(member);
});

// Message event listener
bot.on('message', message=>{
    // Cutting command call to isolate parameters
    let args = message.content.substring(PREFIX.length).split(" ");
    // Commands switch event
    switch(args[0]){
        case 'info':
            bot.commands.get('info').execute(message, args, version, versionState);
            break;
        case 'help':
            bot.commands.get('help').execute(message, args);
        break;
        case 'clear':
            bot.commands.get('clear').execute(message, args);
        break;
        case 'skip':
            bot.commands.get('skip').execute(message, servers);
        break;
        case 'stop':
            bot.commands.get('stop').execute(message, args, servers);
        break;
        case 'play':
            bot.commands.get('play').execute(message, args, servers);
        break;
        case 'kick':
            bot.commands.get('kick').execute(message, args);
        break;
        case 'ban':
            bot.commands.get('ban').execute(message, args);
        break;
        case 'pswd':
            bot.commands.get('pswd').execute(message, args);
        break;
    }
})

// Bot login
bot.login(token);