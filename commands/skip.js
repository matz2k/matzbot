module.exports = {
    name: 'skip',
    description: 'skipping song',
    execute(message, servers){
        var server = servers[message.guild.id];
        if(server.dispatcher) server.dispatcher.end();
        message.channel.send("skipping the song");
    }
}

