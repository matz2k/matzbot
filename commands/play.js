const ytdl = require("ytdl-core");

module.exports = {
    name: 'play',
    description: 'Playing music for the user',
    execute(message, args, servers){
        function play(connection, message){
            var server = servers[message.guild.id];
            server.dispatcher = connection.playStream(ytdl(server.queue[0], {filter: "audioonly"}));
            server.queue.shift();
            server.dispatcher.on("end", function(){
                if (server.queue[0]){
                    play(connection, message);
                }else{
                    connection.disconnect();
                }
            });
        }
        if (!args[1]) return message.channel.send('You need to provide a youtube link')
        if (!args[1].includes("http") || !args[1].includes("youtube") || !args[1].includes("://")) return message.channel.send('You need to specify a valid youtube link as 2nd parameter') 
        if (!message.member.voiceChannel) return message.channel.send('You need to be in a channel to play music')
        if (!servers[message.guild.id]) servers[message.guild.id] = {
            queue: []
        }
        var server = servers[message.guild.id];
        server.queue.push(args[1]);
        if (!message.guild.voiceConnection) message.member.voiceChannel.join().then(function (connection){
            play(connection, message);
        })
    }
}

