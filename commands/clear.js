module.exports = {
    name: 'clear',
    description: 'clear X number of message',
    execute(message, args){
        if(args[1]){
            if (!isNaN(args[1]) && args[1] >= 2 && args[1] <= 100){
                message.channel.bulkDelete(args[1]);
            }else{
                message.channel.send('USAGE : $info <NUMBER>  where NUMBER is between 2 and 100 |   please respect this format')
            }
        }
    }
}


