const Discord = require('discord.js');

const infoembed = new Discord.RichEmbed()
	.setColor('#0099ff')
	.setTitle('Welcome to the Info page')
	.setDescription('Giving you usage exemple')
    .addField('$info <PARAMETER>', 'where PARAMETER can be :')
    .addField('version', 'giving version of the bot and current state')
    .addField('author', 'about the author');


	

module.exports = {
    name: 'info',
    description: 'provide info to user',
    execute(message, args, version, versionState){
        if(!args[1]) return message.channel.send(infoembed)
        switch(args[1]){
            case 'version':
                message.channel.send('version info -> ' + version + ' | version state : ' + versionState);
            break;
            case 'author':
                message.channel.send("Mathieu Stahl is the name of my creator. But please call him Matz !");
            break;

            }
    }
}

