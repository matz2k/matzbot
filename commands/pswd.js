const Discord = require('discord.js');
const generator = require('generate-password');

const helpembed = new Discord.RichEmbed()
	.setColor('#0099ff')
	.setTitle('Welcome to the Password generation tool')
	.setDescription('Giving you usage exemple')
    .addField('$help', 'What command can you use with this bot :')
    .addField('info', 'Find so info about version of the bot, the author, website etc..')
    .addField('clear', 'Clear X numbers of lines in chat');

module.exports = {
    name: 'pswd',
    description: 'Generate a secured password',
    execute(message, args){
        if (!args[1]){
            var password = generator.generate({
                length: 14,
                symbols: true,
                numbers: true
            });
            message.author.send(password);
        }else{
            if(args[1 !== "--help"]){
                var password = generator.generate(args[1]);
                message.author.send(password);
            }else{
                message.channel.send(helpembed);
            }
        }

        message.channel.send('We just secured someone with a new strong password ! Feels good to be safe');
    }
}