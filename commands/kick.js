module.exports = {
    name: 'kick',
    description: 'kick the designated user',
    execute(message, args) {
        const user = message.mentions.users.first();
        if (user) {
            const member = message.guild.member(user);
            if (member) {
                member.kick('You where kicked from the server').then(() => {
                    message.reply(`Sucessfully kicked ${user.tag}`);
                }).catch(err => {
                    message.reply('Bot was unable to kick the target');
                });
            } else {
                message.reply("That target isn\'t in this guild");
            }
        } else {
            message.reply("You need to specify a target");
        }
    }
}