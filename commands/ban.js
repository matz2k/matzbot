module.exports = {
    name: 'ban',
    description: 'ban the designated user',
    execute(message, args) {
        const user = message.mentions.users.first();
        if (user) {
            const member = message.guild.member(user);
            if (member) {
                member.ban({reason: "You were ban from the server"}).then(() =>{
                    message.reply(`Successfully banned target -> ${user.tag}`)
                })
            } else {
                message.reply("That target isn\'t in this guild");
            }
        } else {
            message.reply("You need to specify a target");
        }
    }
}