const Discord = require('discord.js');

const helpembed = new Discord.RichEmbed()
	.setColor('#0099ff')
	.setTitle('Welcome to the Help page')
	.setDescription('Giving you usage exemple')
    .addField('$help', 'What command can you use with this bot :')
    .addField('info', 'Find so info about version of the bot, the author, website etc..')
    .addField('clear', 'Clear X numbers of lines in chat')
    .addField('kick', 'Kick target user')
    .addField('ban', 'Ban target user')
    .addField('play', 'Play the requested youtube link')
    .addField('skip', 'Skip to next song in queue')
    .addField('stop', 'Stop to music player');

module.exports = {
    name: 'help',
    description: 'help the user',
    execute(message, args){
        message.channel.send(helpembed);
    }
}