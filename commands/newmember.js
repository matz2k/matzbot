module.exports = {
    name: 'newmember',
    description: 'Welcome message for new member',
    execute(member) {
        const channel = member.guild.channels.find(channel => channel.name === "welcome");
        if(!channel) return;
        channel.send(`Welcome to our server, ${member} please enjoy your time `)
    }
}