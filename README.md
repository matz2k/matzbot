# MatzBot

Author - Matz

These are the prerequested step to run project:

1. Install FFMPEG on your machine (for windows -> https://www.youtube.com/watch?v=qjtmgCb8NcE, for other platform watch documentation).
2. Install ytdl-core, opusscript, discord via npm install command. Don't forget to add --save if you want the fresh installed lib to be mentionned in package.json.
3. A .conf file must be created contain your bot TOKEN required to connect via API to your bot. You can find it in the discord dev portal you will maybe need to create a fresh bot.
   Be carefull, the TOKEN need to stay hidden from general public, to do so the file is kept at ./.conf and mentionned in the gitignore. So the public can't see your token on git.
   ## the .conf file should look something like this : 
   module.exports = {
    'token': "$discordbottoken"
    };
    <br>
    <br>
   You can generate a new token if your old has been exposed.

   ## Bot prefix
    the bot prefix is set at the top of index.js in the variable called PREFIX change that as you wish. Here we are using $ as default PREFIX.
    ## fast package install
    Just type in "npm install" and all dependencies will be downloaded (ytld-core, discord, etc ..)
    
    ## Basic npm lib install
    $> npm install <package> --save
    ## Run bot
    $> node .

    At start up the bot will tell you in terminal when he is running and ready for use in addition to version and state.
